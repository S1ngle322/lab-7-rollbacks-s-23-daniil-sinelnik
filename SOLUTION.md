## Lab 7 Daniil Sinlenik

I have made migrations

### Part 1
```sql
CREATE DATABASE lab;
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO Player (username, balance) VALUES ('Uncle Bob clean code lol', 200);
INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```

### Part 2
```sql
CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username) NOT NULL,
    product  TEXT REFERENCES Shop(product) NOT NULL,
    amount   INT CHECK (amount >= 0),
    UNIQUE(username, product)
);
```

### EXTRA 1
I would suggest create external table where we can to store successful transactions.
Client need to make IDs for transactions and after that solve the problem in case of software.
Make a cron job that after can execute transactions again.

### EXTRA 2
I would suggest to use already known strategies, we also studied this topic in database course from IU.

There are 3 ways to deal with it:
1. Two-phase commit protocol
2. Three-phase commit protocol
3. Saga-pattern

1) Two-phase commit protocol uses approach as a coordinator node and ensures that all other nodes are ready to make a commit. After that coordinator node ask each other in queue to commit. In case of failure of any node, coordinator node asks them to make a rollback.
2) Three-phase commit protocol is an extension of the first one. It is just simply adds pre-commit phase.
3) ![img.png](img.png)

An e-commerce application that uses this approach would create an order using a choreography-based saga that consists of the following steps:

The Order Service receives the POST /orders request and creates an Order in a PENDING state
It then emits an Order Created event
The Customer Service’s event handler attempts to reserve credit
It then emits an event indicating the outcome
The OrderService’s event handler either approves or rejects the Order

Example was taken from: https://microservices.io/patterns/data/saga.html